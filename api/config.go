package api

import "os"

type SteamConfig struct {
	Filter       string
	Language     string
	ReviewType   string
	PurchaseType string
	NumPerPage   string
	Json         string
}

func GetConfig() SteamConfig {
	return SteamConfig{
		Filter:       "recent",
		Language:     "brazilian",
		ReviewType:   os.Getenv("REVIEW_TYPE"), //"all", "negative", "positive"
		PurchaseType: "steam",
		NumPerPage:   "100",
		Json:         "1",
	}
}
