package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

type Author struct {
	SteamId string `json:"steamId"`
}
type Review struct {
	Id               string `json:"recommendationid"`
	VotedUp          bool   `json:"voted_up"`
	TimestampCreated int64  `json:"timestamp_created"`
	Review           string `json:"review"`
	Author           Author `json:"author"`
}

type ResponseReviews struct {
	Success int      `json:"success"`
	Reviews []Review `json:"reviews"`
	Cursor  string   `json:"cursor"`
}

func GetReviews(game *Game) (*ResponseReviews, error) {
	config := GetConfig()

	query := fmt.Sprintf("http://store.steampowered.com/appreviews/%s?filter=%s&language=%s&review_type=%s&purchase_type=%s&num_per_page=%s&json=1",
		game.Id, config.Filter, config.Language, config.ReviewType, config.PurchaseType, config.NumPerPage)

	if game.Cursor != nil {
		cursorEncoded := url.QueryEscape(*game.Cursor)
		query = query + fmt.Sprintf("&cursor=%s", cursorEncoded)
	}

	response, err := http.Get(query)
	if err != nil {
		log.Print(err.Error())
		return nil, err
	}
	if response.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("expecting status_code=200.")
	}
	rr, err := parseResponse(response)
	if err != nil {
		log.Print(err.Error())
		return nil, err
	}
	game.Cursor = &rr.Cursor
	return rr, nil
}

func parseResponse(response *http.Response) (*ResponseReviews, error) {
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Print(err.Error())
		return nil, err
	}
	var rr ResponseReviews
	err = json.Unmarshal(body, &rr)
	if err != nil {
		log.Print(err.Error())
		return nil, err
	}
	return &rr, err
}
