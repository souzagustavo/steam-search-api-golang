package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"steam-search-api-golang/api"
	"steam-search-api-golang/utils"
	"strings"
	"sync"

	"github.com/joho/godotenv"
	"github.com/robfig/cron"
)

var (
	games []api.Game
	once  sync.Once
)

func getGames() []api.Game {
	once.Do(func() {
		games = []api.Game{
			{Id: "578080", Name: "pubg"},
			{Id: "730", Name: "counter-strike-go"},
			{Id: "271590", Name: "gta-v"},
			{Id: "1174180", Name: "red-dead-redemption-2"},
			{Id: "570", Name: "dota 2"},
		}
	})
	return games
}

func main() {
	godotenv.Load()

	cron := cron.New()
	cron.AddFunc(os.Getenv("CRON_SEARCH"), steamSearch)
	cron.AddFunc(os.Getenv("CRON_BACKUP"), backup)
	cron.Start()

	select {}
}

func steamSearch() {
	games := getGames()
	for g := range games {
		gameSearch(&games[g])
	}
}

func gameSearch(game *api.Game) {
	responseReviews, err := api.GetReviews(game)
	if err != nil {
		log.Println(err.Error())
		return
	}

	f := utils.OpenFile(game.FileName())
	defer f.Close()

	newIDs := []string{}

	fmt.Printf("Response %s: reviews: %d\n", game.Name, len(responseReviews.Reviews))
	for _, review := range responseReviews.Reviews {
		if containsReview(game.FileIdName(), review.Id) {
			continue
		}
		line := utils.FormattedLine(review)

		if _, err := f.WriteString(line); err != nil {
			log.Println(err)
		} else {
			newIDs = append(newIDs, review.Id)
		}
	}
	saveIDs(newIDs, game.FileIdName())
}

func saveIDs(ids []string, fileIdName string) {
	fmt.Printf("New recommendations: %d\n", len(ids))

	f := utils.OpenFile(fileIdName)
	defer f.Close()

	for _, id := range ids {
		if _, err := f.WriteString(fmt.Sprintf("%s\n", id)); err != nil {
			log.Fatal(err)
		}
	}
}

func containsReview(backup, recommendationID string) bool {
	f, err := os.Open(backup)
	if err != nil {
		return false
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		id := strings.TrimSuffix(scanner.Text(), "\n")
		if id == recommendationID {
			return true
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return false
}

func backup() {
	games := getGames()

	for _, game := range games {
		input, err := ioutil.ReadFile(game.FileName())
		if err != nil {
			fmt.Println(err)
			return
		}

		err = ioutil.WriteFile(game.FileBackupName(), input, 0644)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("Game %s: backup completed\n", game.Name)
	}
}
