package utils

import (
	"fmt"
	"log"
	"os"
	"steam-search-api-golang/api"
	"strings"
)

func OpenFile(fileName string) *os.File {
	f, err := os.OpenFile(fileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	return f
}

func FormattedLine(review api.Review) string {
	text := strings.ReplaceAll(review.Review, "\n", " ")
	text = strings.ReplaceAll(text, ";", " ")

	return fmt.Sprintf("%s;%d;%s;%s;%t\n", review.Id, review.TimestampCreated, review.Author.SteamId, text, review.VotedUp)
}
