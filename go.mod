module steam-search-api-golang

go 1.14

require (
	github.com/joho/godotenv v1.4.0
	github.com/robfig/cron v1.2.0
)
