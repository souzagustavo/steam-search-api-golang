## Steam Search API - Golang

Add file `.env` with Environment variables to root folder:
```
#Result
OUTPUT_FOLDER=<PATH>
#Cron
CRON_SEARCH=@every 10s
CRON_BACKUP=@every 1h
#Config
REVIEW_TYPE=all #all, positive or negative
```

How to run:
```
go run main.go
```